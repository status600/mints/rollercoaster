





def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])



import requests
from bs4 import BeautifulSoup

def get_indicator_summaries(symbol):
	url = f"https://www.tradingview.com/symbols/{symbol.upper()}/technicals/"
	response = requests.get(url)
	
	if response.status_code == 200:
		soup = BeautifulSoup(response.text, 'html.parser')
		indicators = soup.find_all(class_='tv-widget-technical-analysis__item')
		for indicator in indicators:
			name = indicator.find(class_='tv-widget-technical-analysis__title').text.strip()
			summary = indicator.find(class_='tv-widget-technical-analysis__signal').text.strip()
			print(f"{name}: {summary}")
	
		
		print ("indicators", indicators)
	
	else:
		print(f"Failed to fetch indicator summaries for {symbol}. Status code:", response.status_code)

# Example usage
get_indicator_summaries('AAPL')