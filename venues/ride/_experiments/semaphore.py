






def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])




import threading
import time

def simultaneously (
	items = [],
	capacity = 2,
	move = lambda : None
):
	#
	#	capacity = 2
	#
	semaphore = threading.Semaphore (capacity)

	# Define a function to process items with semaphore limit
	def process_with_semaphore (item):
		with semaphore:
			move (item)

	# Create threads to process items
	threads = []
	for item in items:
		thread = threading.Thread (
			target = process_with_semaphore, 
			args = (item,)
		)
		
		thread.start ()
		threads.append (thread)

	# Wait for all threads to complete
	for thread in threads:
		thread.join ()
		

def move (item):
	print (f"Processing item: {item}")
	time.sleep (item)
	
	return 2

proceeds = simultaneously (
	items = [1, 1, 1, 1, 1, 1, 1, 1],
	capacity = 4,
	move = move
)

print ("proceeds:", proceeds)