



def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])

import rich
import datetime
from alpaca.data.timeframe import TimeFrame
import rollercoaster.clouds.Alpaca.crypto.retrieve as retrieve_Alpaca_shares
shares = retrieve_Alpaca_shares.wonderfully (
	span = [
		datetime.datetime (2023, 11, 28),
		datetime.datetime (2023, 12, 2)
	],
	interval = TimeFrame.Day,
	symbol_or_symbols = [ "BTC/USD" ]
)

print (shares)

#rich.print_json (data = shares)
