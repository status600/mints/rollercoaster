
'''
	https://tvdb.brianthe.dev/
'''


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import rich
from ships.flow.simultaneous import simultaneously
from tradingview_ta import TA_Handler, Interval, Exchange



def scan_symbol (item):
	symbol = item ["symbol"]
	screener = item ["screener"]
	exchange = item ["exchange"]
	

	def move (interval):
		tesla = TA_Handler (
			symbol = symbol,
			screener = screener,
			exchange = exchange,
			interval = interval
		)
		
		summary = tesla.get_analysis().summary
		
		aggregate = str (summary ["BUY"] - summary ["SELL"])
		if (aggregate [0] != "-"):
			aggregate = "+" + aggregate
		
		
		summary = {
			"interval": interval,
			"technicals": aggregate
		}
		

		
		return summary

	'''
		{
			"TSLA": {
				"interval": "1m",
				"technicals": "16,10,0"
			}
		}
	'''
	technicals = simultaneously (
		items =  [
			Interval.INTERVAL_1_MINUTE,
			Interval.INTERVAL_5_MINUTES,
			Interval.INTERVAL_15_MINUTES,
			Interval.INTERVAL_1_HOUR,
			Interval.INTERVAL_2_HOURS,
			Interval.INTERVAL_4_HOURS,
			Interval.INTERVAL_1_DAY,
			Interval.INTERVAL_1_DAY,
			Interval.INTERVAL_1_WEEK,
			Interval.INTERVAL_1_MONTH
		],
		capacity = 4,
		move = move
	)
	
	technicals_parsed = {}
	for technical in technicals:
		technicals_parsed [ technical ["interval"] ] = technical ["technicals"]
	
	
	#for technical in technicals:
	
	return {
		"symbol": symbol,
		"screener": screener,
		"exchange": exchange,
		"technicals": technicals_parsed
	};



symbols_indicators = simultaneously (
	items = [{
		"symbol": "TSLA",
		"screener": "america",
		"exchange": "NASDAQ"
	},{
		"symbol": "OTLY",
		"screener": "america",
		"exchange": "NASDAQ"
	}],
	capacity = 4,
	move = scan_symbol
)


rich.print_json (data = symbols_indicators)

#proceeds.insert (0, ["New Name", 25, "Male"])

from tabulate import tabulate
#print (tabulate (table_data, headers="firstrow", tablefmt="grid"))

'''
	
				company
	
	interval			1m			5m		15m
				TSLA	16,10,0		14,9,3	11,10,5
				TSLA	16,10,0		14,9,3	11,10,5
'''

from texttable import Texttable
table = Texttable ()

headers = ["", "1m", "5m", "15m", "1h", "2h", "4h", "1d", "1W", "1M" ]
table.header (headers)

rows = []
for symbols_indicator in symbols_indicators:
	symbol = symbols_indicator ["symbol"]
	technicals = symbols_indicator ["technicals"]
	
	rows.append ([ 
		symbol, 
		technicals ["1m"], 
		technicals ["5m"],
		technicals ["15m"], 
		technicals ["1h"], 
		technicals ["2h"], 
		technicals ["4h"], 
		technicals ["1d"], 
		technicals ["1W"], 
		technicals ["1M"]
	])

print(tabulate(rows, headers=headers, tablefmt="grid"))



