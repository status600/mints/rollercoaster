
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])


import yfinance as yf
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Step 1: Get historical stock data using yfinance
ticker_symbol = 'PGNY'  # Example: Apple stock
stock_data = yf.download (
	ticker_symbol, 
	start = '2022-01-01', 
	end = '2023-03-26'
)

# Step 2: Prepare the data for training
stock_data['Date'] = stock_data.index
stock_data.reset_index(drop=True, inplace=True)
stock_data['Next Close'] = stock_data['Close'].shift(-1)  # Shift close price by one day to predict the next day's close
stock_data.dropna(inplace=True)
X = stock_data[['Open', 'High', 'Low', 'Close', 'Volume']]  # Features
y = stock_data['Next Close']  # Target variable

# Step 3: Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split (
	X, y, test_size=0.2, random_state=42)

# Step 4: Train a linear regression model
model = LinearRegression()
model.fit(X_train, y_train)

# Step 5: Make predictions on the test set
predictions = model.predict(X_test)

# Step 6: Evaluate the model
mse = mean_squared_error(y_test, predictions)
print(f'Mean Squared Error: {mse}')

# Step 7: Example prediction for tomorrow's close price
last_data = X.iloc[-1].values.reshape(1, -1)
predicted_close = model.predict(last_data)[0]
print(f'Predicted Close Price for Tomorrow: {predicted_close}')
