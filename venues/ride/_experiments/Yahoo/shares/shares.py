

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])

import rich
from datetime import datetime, timedelta
import rollercoaster.clouds.Yahoo.retrieve as Yahoo_retrieve
prices = Yahoo_retrieve.start (
	symbol = "TAN",
	end_orbit = datetime.today (),
	start_orbit = datetime.today () - timedelta (days = 10),
	interval = "1h"
)

rich.print_json (data = prices)

# print (prices)