

'''
	cd /rollercoaster/structures/ride/__options
'''


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import json
fp = open ("/online/crowns_rollercoaster/mint/tradier.com/online.JSON", "r")
Tradier_API_authorization = json.loads (fp.read ()) ["authorization"]
fp.close ()

#
#	This presumes that the symbol is unique...
#
import rollercoaster.clouds.Tradier.procedures.options.combine as combine_options  
import rollercoaster.stats.aggregate_break_even as aggregate_break_even
break_evens = aggregate_break_even.calc ({
	"expirations": combine_options.presently ({
		"symbol": "SOYB",
		"authorization": Tradier_API_authorization
	})
})

import rich
rich.print_json (data = break_evens)