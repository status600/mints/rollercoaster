

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import json
fp = open ("/online/crowns_rollercoaster/mint/tradier.com/online.JSON", "r")
Tradier_API_authorization = json.loads (fp.read ()) ["authorization"]
fp.close ()


#
#	https://documentation.tradier.com/brokerage-api/reference/exchanges
#
#	Q	NASDAQ OMX
#	N	NYSE
#
#	P	NYSE Arca
#	A	NYSE MKT
#	
import rollercoaster.clouds.Tradier.v1.markets.lookup as lookup_symbol
integral = lookup_symbol.discover ({
	"symbol": "RUN",
	"exchanges": "Q,N",
	"authorization": Tradier_API_authorization
})

import rich
rich.print_json (data = integral)