
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import json
fp = open ("/online/crowns_rollercoaster/mint/tradier.com/online.JSON", "r")
Tradier_API_authorization = json.loads (fp.read ()) ["authorization"]
fp.close ()

'''
	SOYB
	
'''

#
#	This basically assert that there is one symbol....
#
import rollercoaster.clouds.Tradier.v1.markets.lookup as lookup_symbol
integral = lookup_symbol.discover ({
	"symbol": "RUN",
	"exchanges": "Q,N",
	"authorization": Tradier_API_authorization
})

import rollercoaster.clouds.Tradier.procedures.options.combine as combine_options  
the_options_chains = combine_options.presently ({
	"symbol": "RUN",
	"authorization": Tradier_API_authorization
})

#import rich
#rich.print_json (data = the_options_chains)