




def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages',
	'../../stages_pip'
])

import rich

share_price_current = 7.75

option_strike_price = 5
option_ask_price = 3.35

import rollercoaster.treasures.options.multipliers as multipliers
the_multipliers = multipliers.formulate_call (
	share_price_possibilities = [
		30,
		25,
		20,
		15,
		10,
		9,
		8.5,
		8,
		7,
		5,
		0
	],
	share_price_current = share_price_current,

	option_strike_price = option_strike_price,
	option_ask_price = option_ask_price
)


rich.print_json (data = the_multipliers)

multipliers.print_report (the_multipliers)