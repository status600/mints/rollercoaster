


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])



import yfinance as yf

def lookup_option_symbol(stock_symbol, expiration_date):
	try:
		stock = yf.Ticker(stock_symbol)
		options_data = stock.option_chain (expiration_date)
		calls = options_data.calls['contractSymbol'].tolist()
		puts = options_data.puts['contractSymbol'].tolist()
		return {
			'calls': options_data.calls,
			'puts': puts
		}
	except Exception as e:
		return str(e)

# Example usage
stock_symbol = "MSFT"  # Microsoft Corporation
expiration_date = "2026-12-18"  # Example expiration date

option_symbols = lookup_option_symbol (stock_symbol, expiration_date)
print("Call Options:", option_symbols)

'''
for symbol, exchange in option_symbols ['calls']:
	print(f"{symbol} - {exchange}")

print("\nPut Options:")
for symbol, exchange in option_symbols['puts']:
	print(f"{symbol} - {exchange}")
'''