







def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])


'''
	https://www.coingecko.com/en/categories/layer-1
'''

'''
	https://raydium.io/swap/
'''

'''
	Jupiter
	
	MSOLUSD
		https://marinade.finance/
'''

def symbol (the_symbol):
	return {
		"symbol": the_symbol,
		"screener": "crypto",
		"exchange": "COINBASE"
	}

import rich
import rollercoaster.clouds.TradingView.treasure.technicals as TV_treasure_tech
symbols_indicators = TV_treasure_tech.scan_symbols (
	symbols = [
		
	]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech.print_symbols_table (symbols_indicators)