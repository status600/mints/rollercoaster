
'''
	
'''

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])




def symbol (the_symbol, description = "", exchange = "AMEX"):
	return {
		"symbol": the_symbol,
		"screener": "america",
		"exchange": exchange,
		
		"description": description
	}

import rich
import rollercoaster.clouds.TradingView.treasure.technicals as TV_treasure_tech
symbols_indicators = TV_treasure_tech.scan_symbols (
	symbols = [
		symbol ("CANC", "oncology", "NASDAQ"),
		symbol ("CNCR", "cancer, therapeutics", "NASDAQ"),
		symbol ("GNOM", "genetics", "NASDAQ"),
		symbol ("IDNA", "genomics, immunotherapy", "NASDAQ"),
		symbol ("WDNA", "biorevolution"),
	]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech.print_symbols_table (symbols_indicators)