
'''
	
'''

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])

'''
	itinerary:
		data points: 
			1 YR change
			
		current market cap
'''

'''
	itinerary:
		rollercoaster ETFs
'''

def symbol (the_symbol, description = "", exchange = "AMEX"):
	return {
		"symbol": the_symbol,
		"screener": "america",
		"exchange": exchange,
		
		"description": description
	}

import rich
import rollercoaster.clouds.TradingView.treasure.technicals as TV_treasure_tech
symbols_indicators = TV_treasure_tech.scan_symbols (
	symbols = [
		symbol ("CRUZ", "cruises"),
		symbol ("DFEN", "peace"),
		symbol ("FDN", "internet"),
		symbol ("PNQI", "internet", "NASDAQ"),
		symbol ("PSI", "semiconductors"),
		symbol ("GNOM", "genetics"),
		
	
	{
		"symbol": "JETS",
		"screener": "america",
		"exchange": "AMEX",
		
		"description": "airlines"
	},{
		"symbol": "KIE",
		"screener": "america",
		"exchange": "AMEX",
		
		"description": "insurance"
	},{
		"symbol": "MSOS",
		"screener": "america",
		"exchange": "AMEX",
		
		"description": "cannabis"
	},{
		"symbol": "PJP",
		"screener": "america",
		"exchange": "AMEX",
		
		"description": "pharmaceuticals"
	},{
		"symbol": "LABU",
		"screener": "america",
		"exchange": "AMEX",
		
		"descriptions": "biotech x3"
	},{
		"symbol": "VOO",
		"screener": "america",
		"exchange": "AMEX",
		
		"descriptions": ""
	}]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech.print_symbols_table (symbols_indicators)