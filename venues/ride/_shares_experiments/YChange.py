





def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])




import numpy
from datetime import datetime, timedelta
import rollercoaster.clouds.Yahoo.retrieve_change as Yahoo_retrieve_change
change = Yahoo_retrieve_change.perfectly (
	symbol = "DFEN",
	date_1 = datetime.today () - timedelta (days = 365),
	date_2 = datetime.today ()
)


rounded_float = numpy.round(float (change), decimals=3)

print (rounded_float)