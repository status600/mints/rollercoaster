




def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])



import rich
from datetime import datetime, timedelta
import rollercoaster.clouds.TradingView.treasure.technicals_v2 as TV_treasure_tech_v2
symbols_indicators = TV_treasure_tech_v2.scan_symbols (
	changes = [
		[ 
			datetime.today () - timedelta (days = 1),
			datetime.today ()
		]
	],
	symbols = [{
		"symbol": "TSLA",
		"screener": "america",
		"exchange": "NASDAQ",
		"description": "Tesla"
	},{
		"symbol": "OTLY",
		"screener": "america",
		"exchange": "NASDAQ"
	}]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech_v2.print_symbols_table (symbols_indicators)