


def gov_land_rental_amount (
	home_price, 
	tax_multiplier = Fraction (95, 10000)
):
	return home_price * tax_multiplier
	

