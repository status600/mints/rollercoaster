

'''
	from .supertrend_1 import calculate_supertrend
	calculate_supertrend (data, period=7, multiplier=3)
'''

'''
Can you provide me with
the supertrend formula in python3 without pandas 
with input as a list of dicts, 
that adds supertrend and atr to the dicts in the input list,
and the input values are "high", "low", and "close",
with the licensing info written as a comment?


Where the license says:
"copyright ChatGPT and OpenAI"
"This work is subject to the terms of a custom license that is a fork of the MIT license."
Where the license is a custom version of the MIT license
that doesn't use any all caps writing?
'''
# Custom License (Fork of MIT License)

# Copyright (c) ChatGPT and OpenAI

# This work is subject to the terms of a custom license that is a fork of the MIT license.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "software"), to deal
# in the software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the software, and to permit persons to whom the software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the software.

# The software is provided "as is," without warranty of any kind, express or
# implied, including but not limited to the warranties of merchantability,
# fitness for a particular purpose, and noninfringement. In no event shall the
# authors or copyright holders be liable for any claim, damages, or other
# liability, whether in an action of contract, tort or otherwise, arising from,
# out of or in connection with the software or the use or other dealings in
# the software.

def calculate_supertrend(data, period=7, multiplier=3):
    """
    Calculate Supertrend and Average True Range (ATR) without using pandas.
    Adds Supertrend and ATR values to the dicts in the input list.

    Parameters:
        data (list of dict): List of dictionaries where each dictionary represents a day's data with 'high', 'low', and 'close' prices.
        period (int): Supertrend period (default is 7).
        multiplier (float): Multiplier for calculating the average true range (default is 3).

    Returns:
        None
    """
    tr_list = []
    atr_list = []

    for i in range(1, len(data)):
        tr = max(
            data[i]['high'] - data[i]['low'],
            abs(data[i]['high'] - data[i - 1]['close']),
            abs(data[i]['low'] - data[i - 1]['close'])
        )
        tr_list.append(tr)

    atr_list.append(sum(tr_list[:period]) / period)  # Initial ATR value

    for i in range(period, len(tr_list)):
        atr_value = ((period - 1) * atr_list[-1] + tr_list[i]) / period
        atr_list.append(atr_value)

    for i in range(len(atr_list)):
        supertrend = data[i]['close'] + multiplier * atr_list[i]
        data[i]['ATR'] = atr_list[i]
        data[i]['Supertrend'] = supertrend

# Example usage:
# data = [
#     {'high': 50, 'low': 45, 'close': 48},
#     {'high': 55, 'low': 50, 'close': 52},
#     # ... (more data)
# ]
# calculate_supertrend(data, period=7, multiplier=3)
# print(data)
