


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

from ChatGPT_indicators.supertrend_1 import calculate_supertrend


import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import datetime
import rich

print ('CCXT Version:', ccxt.__version__)


# print (ccxt.exchanges)

exchange = ccxt.kraken ()
symbol = "BTC/USDT"


import arrow
def course ():
	timestamp = int (
		datetime.datetime.strptime (
			"2023-12-25 20:00:00+00:00", 
			"%Y-%m-%d %H:%M:%S%z"
		).timestamp () * 1000
	)
	response = exchange.fetch_ohlcv (
		symbol, 
		'15m', 
		timestamp,
		limit = 1000
	)
	
	#pprint (response)

	parsed = []
	for interval in response:	
		UTC_date_string = arrow.get (interval [0]).datetime.isoformat ();
	
		parsed.append ({
			"UTC timestamp": interval [0],
			"UTC date string": UTC_date_string,
			
			"open": interval [1],
			"high": interval [2],
			"low": interval [3],
			"close": interval [4],
			"volume": interval [5]
		})

	return parsed


intervals = course ()

calculate_supertrend (intervals, period=30, multiplier=1.3)

for interval in intervals:
	print (interval)


def plot (intervals):
	import plotly.graph_objects as go
	import pandas as pd
	from datetime import datetime

	from plotly.subplots import make_subplots
	import plotly.graph_objects as go

	#fig = make_subplots(rows=2, cols=1)
	# fig = make_subplots(rows=2, cols=1, row_heights = [0.7, 0.3])
	# fig = make_subplots(rows=1, cols=2, column_widths=[0.7, 0.3])

	fig = make_subplots(
		rows=2, 
		cols=1, 
		shared_xaxes=True, 
		vertical_spacing=0.03, 
		subplot_titles=('OHLC', 'Volume'), 
		row_width=[0.2, 0.7]
	)

	df = pd.DataFrame.from_dict (intervals)

	fig.append_trace ( 
		go.Candlestick (
			x = df ['UTC date string'],
			
			open = df ['open'],
			high = df ['high'],
			low = df ['low'],
			close = df ['close']
		),
		row = 1,
		col = 1
	)
	
	fig.append_trace ( 
		go.Scatter (
			x = df ['UTC date string'],
			
			y = df ['Supertrend'],

			mode = 'lines',
			
			name = 'Supertrend'
		),
		row = 1,
		col = 1
	)

	fig.add_trace(go.Bar(x=df['UTC date string'], y=df['volume'], showlegend=False), row=2, col=1)

	# Do not show OHLC's rangeslider plot 
	fig.update (layout_xaxis_rangeslider_visible = False)



	#fig.update_layout(height=600, width=600, title_text="Stacked Subplots")
	fig.show ()

plot (intervals)