
'''
	Scikit:
		Neighbors:
			https://scikit-learn.org/stable/modules/classes.html#module-sklearn.neighbors
			
		KNN:
			https://scikit-learn.org/stable/auto_examples/neighbors/plot_classification.html#sphx-glr-auto-examples-neighbors-plot-classification-py

	KNN:
		https://www.datacamp.com/tutorial/k-nearest-neighbor-classification-scikit-learn
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../stages',
	'../../../stages_pip'
])


from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

# Load the Iris dataset
iris = load_iris ()
X = iris.data
y = iris.target

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split (X, y, test_size = 0.2, random_state = 42)

# Standardize the features
scaler = StandardScaler ()
X_train = scaler.fit_transform (X_train)
X_test = scaler.transform (X_test)

# Initialize the KNN classifier
k = 3  # Number of neighbors
knn_classifier = KNeighborsClassifier (n_neighbors = k)

# Train the classifier
knn_classifier.fit (X_train, y_train)

# Make predictions
y_pred = knn_classifier.predict (X_test)

# Calculate accuracy
accuracy = accuracy_score (y_test, y_pred)
print ("Accuracy:", accuracy)


#
#
#
