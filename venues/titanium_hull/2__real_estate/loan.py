

'''
	https://github.com/austinmcconnell/mortgage
'''

'''
	300000 / 30
	
		10000 per year
'''

'''
	taxes:
		multiplier: 0.009
	
		300000 * 0.009 = 2700
			
	mortgage:
		multiplier: 0.08
		
		300000 * 0.08 = 24000.0
		290000 
		
		200000 * 0.08 = 16000.0
		
		100000 * 0.08 =  8000.0
'''	


'''
	{
		"name": "30 year constant",
		
		"mortgage": {
			"multiplier": "0.08"
		},
		"tax": {
			"mutliplier": "0.009"
		},
		"HOA": {
			"flat": "300"
		},
		"insurance": {
			"flat": ""
		},
		
		"dates": [{
			"year": 1,
			"tax": {
				"send": ""
			},
			"mortgage": {
				"premium": ""
			}
		},{
			"year": 2,
			"tax": 
		}]
	}
	
'''

import rich
from fractions import Fraction



'''
	example: 350,000 * 0.0095
'''
def loan_premium (
	loan_remaining, 
	tax_multiplier = Fraction (8, 100)
):
	return home_price * tax_multiplier

'''			
	
'''
def calculate_yearly_payments (
	loan = "", 
	yearly_loan_multiplier = "", 
	yearly_loan_send = "",
	
	span = ""
):
	yearly_payments = []
	
	loan_remaining = loan
	
	aggregate_send = 0
	
	year_number = 1
	for year in range (span):
		loan_remaining -= yearly_loan_send
		loan_premium = loan_remaining * yearly_loan_multiplier
		
		send = str (
			Fraction (loan_premium) +
			Fraction (yearly_loan_send)
		)
		
		aggregate_send += Fraction (send)
		
		rich.print_json (data = {
			"year": year_number,
			
			"send": send,

			"loan": {
				"remaining": str (loan_remaining),
				"premium": str (loan_premium),
				"redution": str (yearly_loan_send)
			}
		})
        
		year_number += 1
		
	yearly_amount = Fraction (aggregate_send, span)
	monthly_amount = Fraction (
		Fraction (aggregate_send, span),
		12
	)
	
	
	print ("aggregate_send:", aggregate_send)
	print ("yearly amount:", str (yearly_amount))
	print ("monthly amount:", str (monthly_amount))
		
	
		
	return yearly_payments




loan = 291000
yearly_loan_send = 10000
yearly_loan_multiplier = Fraction (8,100)  

span = 30

monthly_payment = calculate_yearly_payments (
	loan = loan, 
	
	yearly_loan_multiplier = yearly_loan_multiplier, 
	yearly_loan_send = yearly_loan_send,
	
	span = span
)

