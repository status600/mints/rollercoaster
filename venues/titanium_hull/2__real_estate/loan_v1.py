

'''
	300000 / 30
	
		10000 per year
'''

'''
	taxes:
		multiplier: 0.009
	
		300000 * 0.009 = 2700
			
	mortgage:
		multiplier: 0.08
		
		300000 * 0.08 = 24000.0
		290000 
		
		200000 * 0.08 = 16000.0
		
		100000 * 0.08 =  8000.0
'''	

'''
	variables:
		[ ] multiplier
			( ) constant
			( ) fluctuating
			
		[ ] when are interest rates calculated
			( ) monthly
			( ) yearly
			
		[ ] when you need to completelly pay back the mortgage
			( ) after 30 years
				
					maybe you're required to pay back
					at least 1/30 per year.
					
						(1/360)
					
			
			( ) after 15 years
				
					maybe you're required to pay back
					at least 1/30 per year.
			
'''



'''
	{
		"name": "30 year constant",
		
		"mortgage": {
			"multiplier": "0.08"
		},
		"tax": {
			"mutliplier": "0.009"
		},
		"HOA": {
			"flat": "300"
		},
		"insurance": {
			"flat": ""
		},
		
		"dates": [{
			"year": 1,
			"tax": {
				"send": ""
			},
			"mortgage": {
				"premium": ""
			}
		},{
			"year": 2,
			"tax": 
		}]
	}
	
'''

import rich
from fractions import Fraction

'''
	example: 350,000 * 0.0095
'''
def gov_land_rental_amount (
	home_price, 
	tax_multiplier = Fraction (95, 10000)
):
	return home_price * tax_multiplier
	
def company_land_rental_amount ():
	return;

'''
	example: 350,000 * 0.0095
'''
def loan_premium (
	loan_remaining, 
	tax_multiplier = Fraction (8, 100)
):
	return home_price * tax_multiplier

'''			
	
'''
def calculate_yearly_payments (
	home_price = "",
	
	loan = "", 
	yearly_loan_multiplier = "", 
	yearly_loan_send = "",
	
	span = ""
):
	yearly_payments = []
	
	loan_remaining = loan
	
	year_number = 1
	for year in range (span):
		loan_remaining -= yearly_loan_send
		loan_premium = loan_remaining * yearly_loan_multiplier
		
		send = str (
			Fraction (gov_land_rental_amount (home_price)) +
			Fraction (loan_premium) +
			Fraction (yearly_loan_send)
		)
		
		rich.print_json (data = {
			"year": year_number,
			
			"send": send,
			
			"tax": {
				"send": str (gov_land_rental_amount (home_price))
			},
			"mortage": {
				"remaining": str (loan_remaining),
				"premium": str (loan_premium),
				"reduce":  str (yearly_loan_send)
			},
			"HOA": {
				"send": ""
			},
			"land rental": {
				"send": ""
			}
		})
        
		year_number += 1
		
		
	return yearly_payments


#
#	loan
#
home_price = 350000

loan = 300000
yearly_loan_send = 10000
yearly_loan_multiplier = Fraction (8,100)  

span = 30

monthly_payment = calculate_yearly_payments (
	home_price = home_price,
	
	loan = loan, 
	yearly_loan_multiplier = yearly_loan_multiplier, 
	yearly_loan_send = yearly_loan_send,
	
	span = span
)

