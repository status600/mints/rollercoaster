






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])


import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

fp = open ("/online ellipsis/cortex/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()


exchange = ccxt.kraken ()
symbol = "BTC/USDT"


import essentials
jumps = essentials.retrieve_stats (
	symbol = symbol,
	exchange = ccxt.kraken ()
)

import rollercoaster.rides.season_2.pecdency as pecdency_tap
pecdency_tap.calc (
	places = jumps,
	
	spans = 50,
	multiplier = 0.5
)

rich.print_json (data = jumps)

import pandas
jumps_DF = pandas.DataFrame (jumps)

import pandas_ta
SMA_10 = pandas_ta.sma (jumps_DF ["close"], length = 10)
jumps_DF = jumps_DF.join (SMA_10)

import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
chart = CCXT_OHLCV_candles.show (
	DF = jumps_DF
)

def plot_LB (jumps_DF_1):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 ["pecdency LB"], 
			line = dict (
				color = 'white', 
				width = 1
			)
		),
		row = 1,
		col = 1
	)
	
def plot_UB (jumps_DF_1):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 ["pecdency UB"], 
			line = dict (
				color = 'white', 
				width = 1
			)
		),
		row = 1,
		col = 1
	)
	
def plot_pecdency (jumps_DF_1):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 ["pecdency 1"], 
			line = dict (
				color = 'white', 
				width = 1
			)
		),
		row = 1,
		col = 1
	)
	
#plot_LB (jumps_DF)
#plot_UB (jumps_DF)
plot_pecdency (jumps_DF)

chart.show ()






#