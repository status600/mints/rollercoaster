

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


symbol = "BTC/USDT"

import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import datetime
import rich
import arrow

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

exchange = ccxt.kraken ()
#exchange = ccxt.coinbase ()
exchange = ccxt.cryptocom ()
exchange = ccxt.kucoin ()


import ccxt

import rollercoaster.clouds.CCXT.OHLCV as CCXT_OHLCV_intervals
response = exchange.fetch_ohlcv (
	symbol, 
	'15m', 
	int (
		datetime.datetime.strptime (
			"2023-12-25 20:00:00+00:00", 
			"%Y-%m-%d %H:%M:%S%z"
		).timestamp () * 1000
	),
	limit = 100
)


import pandas
df = pandas.DataFrame (response, columns=['date', 'open', 'high', 'low', 'close', 'volume'])
df['date'] = pandas.to_datetime(df['date'], unit='ms')

print (df)

from stock_indicators import Quote

quotes_list = [
    Quote(d,o,h,l,c,v) 
    for d,o,h,l,c,v 
    in zip(df['date'], df['open'], df['high'], df['low'], df['close'], df['volume'])
]

from stock_indicators import indicators



# calculate 14-period ATR
results = indicators.get_atr (quotes_list, 14)

print (results)


'''
from rollercoaster.rides.season_1.supertrend.ST_1_ChatGPT import calculate_supertrend
calculate_supertrend (intervals, period = 7, multiplier = 3)

from rich import print_json
print_json (data = intervals)

print ("intervals count:", len (intervals))

import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
CCXT_OHLCV_candles.show (
	intervals = intervals
)
'''


