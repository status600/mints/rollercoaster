



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

symbol = "BTC/USDT"

import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import datetime
import rich
import arrow
import json

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)


def retrieve_ellipsis ():
	import json
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;

ellipsis = retrieve_ellipsis ()

exchange = ccxt.kraken ()



import ccxt
import pandas


	
import essentials
'''
OHLCV_DF = essentials.retrieve_OHLCV (
	symbol = "BTC-USD",
	exchange = ccxt.kraken ()
)
'''
OHLCV_DF = essentials.retrieve_OHLCV_2 (
	ellipsis = ellipsis,
	product_id = "FET-USD"
)


print (OHLCV_DF)

ST_length = 50;
ST_multiplier = 2.0;

ST_direction = f"SUPERTd_{ str (ST_length) }_{ str (ST_multiplier) }"

import pandas_ta
supertrend_DF = pandas_ta.supertrend (
	OHLCV_DF ['high'], 
	OHLCV_DF ['low'], 
	OHLCV_DF ['close'], 
	
	length = ST_length, 
	multiplier = ST_multiplier
)

SMA_10 = pandas_ta.sma (OHLCV_DF ["close"], length = 10)

DF_1_1 = OHLCV_DF.join (SMA_10)
DF_2 = DF_1_1.join (supertrend_DF)


''''
import rollercoaster.rides.season_1.supertrend as supertrend_indicator
supertrend_indicator.calc (
	places = jumps,
	spans = 4
)
'''

#from rich import print_json
#print_json (data = DF_2.to_dict ('records'))
#print_json (data = supertrend_DF.to_dict ('records'))


def form_the_figure ():
	return;

'''
	This configures the candles figure.
'''
import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
chart = CCXT_OHLCV_candles.show (
	DF = DF_2
)

'''
	SMA 10
'''
import plotly.graph_objects as go
chart.add_trace (
	go.Scatter (
		x = DF_2 ['UTC date string'], 
		y = DF_2 ["SMA_10"], 
		line = dict (color = 'orange', width = 1)
	),
	row = 1,
	col = 1
)

'''
	Super Trend,
		Color of candles
'''
import numpy
chart.add_trace (
	go.Scatter (
		x = DF_2 ['UTC date string'],
		y = DF_2 ["close"],
		
		marker_color = numpy.select (
			[
				DF_2 [ ST_direction ] == -1, 
				DF_2 [ ST_direction ] == 1
			], 
			["orange", "purple"], 
			"rgba(0,0,0,0)"
		),
		
		mode = "markers",
		#marker_color = "black",
		yaxis = "y2",
		name = "Bubble"
	),
	row = 1, 
	col = 1
)

import pandas as pd


win_rate = 1;

annotations = []
shapes = []

amounts = []

previous = None
for index, row in DF_2.iterrows ():
	direction = row [ ST_direction ]
	UTC_date_string = row ['UTC date string']
	if (previous in [ -1, 1 ] and direction != previous):		
		if (previous == -1):
			text = '+'
		else:
			text = '-'
			
		amounts.append ([ 
			previous * -1,
			row ["close"]
		])
		
		print (
			"change of direction:", 
			previous * -1, 
			row ['UTC date string'], 
			row ["close"]
		)
		
		shapes.append (
			dict (
				x0 = row ['UTC date string'], 
				x1 = row ['UTC date string'],
				
				y0 = 0, 
				y1 = 1, 
				
				xref = 'x', 
				yref = 'paper',
				
				line_width = 2
			)
		)

		annotations.append (
			dict (
				x = row ['UTC date string'], 
				y = 0.05, 
				
				xref = 'x', 
				yref = 'paper',
				xanchor = 'left',
				
				showarrow = False, 
				 
				text = text
			)
		)
		
		previous = direction;
	
	else:
		previous = direction;

import essentials
essentials.calculate_win_rate (amounts);

chart.update_layout (
	shapes = shapes,
    annotations = annotations
)


chart.show ()

print (f"[{ UTC_date_string }]", direction)
