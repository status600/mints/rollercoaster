
import ships.flow.demux_mux2 as demux_mux2

def retrieve_stats (
	symbol = "",
	exchange = ""
):
	import ccxt
	import datetime
	import pandas

	import rollercoaster.clouds.CCXT.OHLCV as CCXT_OHLCV_intervals
	jumps = CCXT_OHLCV_intervals.retrieve (
		symbol = symbol,
		exchange = exchange,
		from_time = int (
			datetime.datetime.strptime (
				"2023-12-25 20:00:00+00:00", 
				"%Y-%m-%d %H:%M:%S%z"
			).timestamp () * 1000
		),
		limit = 300,
		
		span = '15m'
	)
	
	#OHLCV_DF = pandas.DataFrame (jumps)
	#return OHLCV_DF;
	
	return jumps;

def retrieve_OHLCV_2 (
	ellipsis,
	product_id = ""
):
	from datetime import datetime, timezone, timedelta
	UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
	start = int ((datetime.utcnow().replace(tzinfo=timezone.utc) - timedelta (days = 3)).timestamp ())

	from operator import itemgetter
	import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
	OHLCV_DF = Coinbase_API_product_candles.proposal (
		key_name = ellipsis ["name"],
		key_secret = ellipsis ["privateKey"],
		
		#product_id = "FET-USD",
		#product_id = "BTC-USD",
		
		product_id = product_id,
		

		#granularity = "ONE_HOUR",	
		#granularity = "THIRTY_MINUTE",	
		granularity = "FIFTEEN_MINUTE",
		#granularity = "FIVE_MINUTE",
		
		start = start,
		end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
	)

	return OHLCV_DF;


def calculate_win_rate (amounts):
	win_rate = 1
	trade_count = 0
	
	add_to_trade_count = False;
	
	last_index = len (amounts) - 1
	s = 1
	while (s <= last_index):
		amount = amounts [s]
		
		direction = amount [0]
		
		price_1 = amounts [s - 1] [1]
		price_2 = amount [1]
		
		if (direction == -1):
			add_to_trade_count = True;
		
			incline_change = (price_2 / price_1)
		
			win_rate = win_rate * incline_change
			print ("win rate:", win_rate)
			
		if (add_to_trade_count):
			print ("trade_count += 1", amount) 
			trade_count += 1
		
		
		#print (amount)
	
		
	
		s += 1

	print ()
	print ("win rate:", win_rate)
	print ("trade_count:", trade_count)
	print ()

	return;