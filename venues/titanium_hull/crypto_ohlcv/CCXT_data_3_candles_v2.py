







def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


symbol = "BTC/USDT"

import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import plotly.graph_objects as go
import datetime
import rich
import arrow
import pandas_ta as ta
import pandas
import numpy

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

exchange = ccxt.kraken ()
#exchange = ccxt.coinbase ()
exchange = ccxt.cryptocom ()



import ccxt

import rollercoaster.clouds.CCXT.OHLCV as CCXT_OHLCV_intervals
jumps = CCXT_OHLCV_intervals.retrieve (
	symbol = symbol,
	exchange = ccxt.kraken (),
	from_time = int (
		datetime.datetime.strptime (
			"2023-12-25 20:00:00+00:00", 
			"%Y-%m-%d %H:%M:%S%z"
		).timestamp () * 1000
	),
	limit = 200,
	
	span = '1h'
)

DF = pandas.DataFrame (jumps)

print ("DF:", DF)

supertrend_DF = ta.supertrend (
	DF ['high'], 
	DF ['low'], 
	DF ['close'], 
	length = 7, 
	multiplier = 3
)


sma10 = ta.sma (DF ["close"], length = 10)

DF_1_1 = DF.join (sma10)
DF_2 = DF_1_1.join (supertrend_DF)

print (sma10)


import rollercoaster.clouds.CCXT.OHLCV.candles_v2 as CCXT_OHLCV_candles_v2
figure = CCXT_OHLCV_candles_v2.show (
	DF = DF
)

figure.add_trace (
	go.Scatter (
		x = DF_2 ['UTC date string'],
		y = DF_2 ["close"],
		
		marker_color = numpy.select (
			[
				DF_2 ["SUPERTd_7_3.0"] == -1, 
				DF_2 ["SUPERTd_7_3.0"] == 1
			], 
			["orange", "purple"], 
			"rgba(0,0,0,0)"
		),
		
		mode = "markers",
		#marker_color = "black",
		yaxis = "y2",
		name = "Bubble"
	)
)


annotations = []
shapes = []

previous = None
for index, row in DF_2.iterrows ():
	print(row ['SUPERTd_7_3.0'])

	direction = row ['SUPERTd_7_3.0']
	if (previous in [ -1, 1 ] and direction != previous):
		print ("change of direction")
		print (row ['UTC date string'], row ["close"])
		
		shapes.append (
			dict (
				x0 = row ['UTC date string'], 
				x1 = row ['UTC date string'],
				
				y0 = 0, 
				y1 = 1, 
				
				xref = 'x', 
				yref = 'paper',
				
				line_width = 2
			)
		)

		annotations.append (
			dict (
				x=row ['UTC date string'], 
				y=0.05, 
				
				xref='x', 
				yref='paper',
				xanchor='left',
				
				showarrow = False, 
				 
				text = '+'
			)
		)


		'''
		chart.add_trace (
			chart.add_annotation (
				x = row ['UTC date string'],
				#y = row ["close"],
				
				text = 'text?',
				showarrow = True,
				yshift = 10
			),
			row = 1, 
			col = 1
		)
		'''
	
	previous = direction;


figure.update_layout (
	shapes = shapes,
	annotations = annotations
)

'''
figure.update_layout (
	shapes = [
		dict (
			x0 = '2024-01-03T03:30:00+00:00', 
			x1 = '2024-01-03T03:30:00+00:00', 
			y0 = 0, 
			y1 = 1, 
			xref = 'x', 
			yref = 'paper',
			line_width = 2
		)
	],

    annotations = [
		dict (
			x='2024-01-03T03:30:00+00:00', 
			y=0.05, 
			
			xref='x', 
			yref='paper',
			xanchor='left',
			
			showarrow = False, 
			 
			text = '+'
		)
	]
)
'''

figure.show ()