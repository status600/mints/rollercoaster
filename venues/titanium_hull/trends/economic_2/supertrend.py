






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

def retrieve_ellipsis ():
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;

def retrieve_Coinbase_trading_data (
	ellipsis
):
	product_id = "FET-USD"

	from datetime import datetime, timezone, timedelta
	UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
	start = int ((datetime.utcnow().replace(tzinfo=timezone.utc) - timedelta (days = 3)).timestamp ())

	from operator import itemgetter
	import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
	OHLCV_DF = Coinbase_API_product_candles.proposal (
		key_name = ellipsis ["name"],
		key_secret = ellipsis ["privateKey"],
		
		product_id = product_id,
		granularity = "FIFTEEN_MINUTE",
		
		start = start,
		end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
	)
	
	return OHLCV_DF



def plot_2_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [venture], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 2,
		col = 1
	)
	

def plot_1_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [ venture ], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)



def build_chart (OHLCV_DF):
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)
	
	return chart;


def main ():
	OHLCV_DF = retrieve_Coinbase_trading_data (
		ellipsis = retrieve_ellipsis ()
	)

	chart = build_chart (
		OHLCV_DF = OHLCV_DF
	);
	
	import rollercoaster.rides.season_3.supertrend as supertrend
	OHLCV_DF ['supertrend'] = supertrend.calc (OHLCV_DF, period=7, multiplier=1.5)
	plot_1_venture (chart, OHLCV_DF, "supertrend", color = palette ["jade"])
	
	
	import rollercoaster.rides.season_1.RSI as RSI
	OHLCV_DF ['RSI'] = RSI.calc (OHLCV_DF)
	plot_2_venture (chart, OHLCV_DF, "RSI", color = palette ["jade"])
	
	print (OHLCV_DF)
	
	'''
		
	plot_2_venture (chart, OHLCV_DF, "PSA", color = palette ["opal"])	

	plot_1_venture (chart, OHLCV_DF, "PSR UB", color = palette ["opal"])	
	plot_1_venture (chart, OHLCV_DF, "PSR LB", color = palette ["jade"])	
	plot_1_venture (chart, OHLCV_DF, "PSR", color = "rgb(255,100,100)")	
	'''

	chart.show ()

main ()







#