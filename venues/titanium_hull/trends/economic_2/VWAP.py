






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

def retrieve_ellipsis ():
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;

def retrieve_Coinbase_trading_data ():
	# product_id = "FET-USD"
	product_id = "TIA-USD"

	import essentials
	OHLCV_DF = essentials.retrieve_Coinbase_OHLCV (
		ellipsis = retrieve_ellipsis (),
		product_id = product_id
	)
	
	#import pandas
	#OHLCV_DF_DF = pandas.DataFrame (OHLCV_DF)
	
	return OHLCV_DF


def plot_2_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [venture], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 2,
		col = 1
	)
	

def plot_1_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [ venture ], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)



def build_chart (OHLCV_DF):
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)
	
	return chart;


def main ():
	OHLCV_DF = retrieve_Coinbase_trading_data ()

	chart = build_chart (
		OHLCV_DF = OHLCV_DF
	);
	
	'''
	import rollercoaster.rides.season_1.RSI as RSI
	OHLCV_DF ['RSI'] = RSI.calc (OHLCV_DF)
	'''
	
	import rollercoaster.rides.season_1.VWAP as VWAP
	OHLCV_DF ['VWAP'] = VWAP.calc (OHLCV_DF)
	plot_2_venture (chart, OHLCV_DF, "VWAP", color = palette ["jade"])
	
	print (OHLCV_DF)
	
	'''
		
	plot_2_venture (chart, OHLCV_DF, "PSA", color = palette ["opal"])	

	plot_1_venture (chart, OHLCV_DF, "PSR UB", color = palette ["opal"])	
	plot_1_venture (chart, OHLCV_DF, "PSR LB", color = palette ["jade"])	
	plot_1_venture (chart, OHLCV_DF, "PSR", color = "rgb(255,100,100)")	
	'''

	chart.show ()

main ()







#