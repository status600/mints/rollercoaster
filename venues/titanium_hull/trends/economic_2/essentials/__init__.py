
import ships.flow.demux_mux2 as demux_mux2



import ccxt
import datetime
import pandas
import rollercoaster.clouds.CCXT.OHLCV as CCXT_OHLCV_intervals
def retrieve_stats (
	symbol = "",
	exchange = ""
):
	return CCXT_OHLCV_intervals.retrieve (
		symbol = symbol,
		exchange = exchange,
		from_time = int (
			datetime.datetime.strptime (
				"2023-12-25 20:00:00+00:00", 
				"%Y-%m-%d %H:%M:%S%z"
			).timestamp () * 1000
		),
		limit = 300,
		
		span = '15m'
	)
	
	

def retrieve_Coinbase_OHLCV (
	ellipsis,
	
	#product_id = "FET-USD",
	#product_id = "BTC-USD",
	product_id = "",
	
	#granularity = "ONE_HOUR",	
	#granularity = "THIRTY_MINUTE",	
	granularity = "FIFTEEN_MINUTE",
):
	from datetime import datetime, timezone, timedelta
	UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
	start = int ((datetime.utcnow().replace(tzinfo=timezone.utc) - timedelta (days = 3)).timestamp ())

	from operator import itemgetter
	import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
	OHLCV_DF = Coinbase_API_product_candles.proposal (
		key_name = ellipsis ["name"],
		key_secret = ellipsis ["privateKey"],
		
		product_id = product_id,
		granularity = granularity,
		
		start = start,
		end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
	)

	return OHLCV_DF;


