








def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

Coinbase_key_path = "/offline/bullion_safes/mint/Coinbase.com/coinbase_cloud_api_key--Awareness-1.JSON"

import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

from operator import itemgetter
import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles


print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

def retrieve_ellipsis ():
	fp = open (Coinbase_key_path, "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;

def retrieve_Coinbase_trading_data ():
	product_id = "FET-USD"

	import essentials
	OHLCV_DF = essentials.retrieve_Coinbase_OHLCV (
		ellipsis = retrieve_ellipsis (),
		product_id = product_id
	)
	
	#import pandas
	#OHLCV_DF_DF = pandas.DataFrame (OHLCV_DF)
	
	return OHLCV_DF


def plot_2_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [venture], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 2,
		col = 1
	)
	

def plot_1_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [ venture ], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)



def build_chart (OHLCV_DF):
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)
	
	return chart;


def main ():
	produc_IDs = [
		"FET-USD"
	]

	OHLCV_DF = Coinbase_API_product_candles.retrieve_Coinbase_OHLCV (
		ellipsis = retrieve_ellipsis (),
		product_id = "FET-USD"
	)

	chart = build_chart (
		OHLCV_DF = OHLCV_DF
	);
	
	import rollercoaster.rides.season_1.RSI as RSI
	OHLCV_DF ['RSI'] = RSI.calc (OHLCV_DF)
	plot_2_venture (chart, OHLCV_DF, "RSI", color = palette ["jade"])
	
	print (OHLCV_DF)
	
	'''
	plot_2_venture (chart, OHLCV_DF, "PSA", color = palette ["opal"])	
	plot_1_venture (chart, OHLCV_DF, "PSR UB", color = palette ["opal"])	
	plot_1_venture (chart, OHLCV_DF, "PSR LB", color = palette ["jade"])	
	plot_1_venture (chart, OHLCV_DF, "PSR", color = "rgb(255,100,100)")	
	'''

	chart.show ()

main ()







#