



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

import json
fp = open ("/online ellipsis/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()

from datetime import datetime, timezone, timedelta
start_date = int ((datetime.utcnow ().replace (tzinfo = timezone.utc) - timedelta (days = 1)).timestamp ())
start_date = datetime.utcfromtimestamp (start_date).isoformat () + "Z"

print ('start_date:', start_date)

request_query_params = "?" + "&".join ([
	f"start_date={ start_date }",
	f"order_side=BUY"
])

import rollercoaster.clouds.Coinbase.API as Coinbase_API
proceeds = Coinbase_API.proposal (
	key_name = ellipsis ["name"],
	key_secret = ellipsis ["privateKey"],
	
	request_method = "GET",
	request_path = "/api/v3/brokerage/orders/historical/batch",
	request_query_params = request_query_params
)



import rich

#print (proceeds)

orders = proceeds ["orders"]
orders.reverse ()

#rich.print_json (data = orders)

for order in orders:
	rich.print_json (data = {
		"client_order_id": order ["client_order_id"],
		"product_id": order ["product_id"],
		"side": order ["side"],
		"status": order ["status"],
		"order_type": order ["order_type"],
		"created_time": order ["created_time"],
		"last_fill_time": order ["last_fill_time"],
		"total_value_after_fees": order ["total_value_after_fees"]
	})
	
	#rich.print_json (data = order)
	