

'''
	https://api.coinbase.com/api/v3/brokerage/products
	https://api.coinbase.com/api/v3/brokerage/products/{product_id}/candles
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import json
import rich
fp = open ("/online ellipsis/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()


from datetime import datetime, timezone, timedelta
UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
start = int ((datetime.utcnow().replace(tzinfo=timezone.utc) - timedelta (days = 3)).timestamp ())


from operator import itemgetter
import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
OHLCV_DF = Coinbase_API_product_candles.proposal (
	key_name = ellipsis ["name"],
	key_secret = ellipsis ["privateKey"],
	
	#product_id = "FET-USD",
	product_id = "BTC-USD",

	#granularity = "ONE_HOUR",	
	#granularity = "THIRTY_MINUTE",	
	granularity = "FIFTEEN_MINUTE",
	#granularity = "FIVE_MINUTE",
	
	start = start,
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
)


ST_length = 50;
ST_multiplier = 2.0;
ST_direction = f"SUPERTd_{ str (ST_length) }_{ str (ST_multiplier) }"

import pandas_ta
supertrend_DF = pandas_ta.supertrend (
	OHLCV_DF ['high'], 
	OHLCV_DF ['low'], 
	OHLCV_DF ['close'], 
	
	length = ST_length, 
	multiplier = ST_multiplier
)


DF = OHLCV_DF.join (supertrend_DF)

def form_the_figure ():
	return;

'''
	This configures the candles figure.
'''
import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
chart = CCXT_OHLCV_candles.show (
	DF = DF
)


'''
	Super Trend,
		Color of candles
'''
import numpy
import plotly.graph_objects as go
chart.add_trace (
	go.Scatter (
		x = DF ['UTC date string'],
		y = DF ["close"],
		
		marker_color = numpy.select (
			[
				DF [ ST_direction ] == -1, 
				DF [ ST_direction ] == 1
			], 
			[ "orange", "purple" ], 
			"rgba(0,0,0,0)"
		),
		
		mode = "markers",
		#marker_color = "black",
		yaxis = "y2",
		name = "Bubble"
	),
	row = 1, 
	col = 1
)



annotations = []
shapes = []

previous = None
for index, row in DF.iterrows ():
	#print(row [ ST_direction ])

	direction = row [ ST_direction ]
	UTC_date_string = row ['UTC date string']
	if (previous in [ -1, 1 ] and direction != previous):
		#print ("change of direction")
		#print (row ['UTC date string'], row ["close"])
		
		shapes.append (
			dict (
				x0 = row ['UTC date string'], 
				x1 = row ['UTC date string'],
				
				y0 = 0, 
				y1 = 1, 
				
				xref = 'x', 
				yref = 'paper',
				
				line_width = 2
			)
		)

		annotations.append (
			dict (
				x = row ['UTC date string'], 
				y = 0.05, 
				
				xref = 'x', 
				yref = 'paper',
				xanchor = 'left',
				
				showarrow = False, 
				 
				text = '+'
			)
		)

	
	previous = direction;


chart.update_layout (
	shapes = shapes,
    annotations = annotations
)



chart.show ()

