

'''
	https://docs.alpaca.markets/reference/stockbarsingle-1
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages',
	'../../stages_pip'
])


import json
fp = open ("/online/vaccines_rollercoaster/mint/alpaca.markets/paper.JSON", "r")
paper = json.loads (fp.read ())
fp.close ()

import rollercoaster.gadgets.clock.UTC.current as UTC_current
current_time = UTC_current.discover ()

import rich


'''

'''
import rollercoaster.clouds.Alpaca._data_API.v2.stock.symbol.bars as Alpaca_bars
response = Alpaca_bars.retrieve (
	symbol = 'VANI',
	params = {
		"timeframe": "1Min",
		"adjustment": "all"
	},
	headers = {
		"APCA-API-KEY-ID": paper ["key"],
		"APCA-API-SECRET-KEY": paper ["secret"]
	}
)

spans = response ["JSON"] ["bars"]
spans_parsed = []
for span in spans:
	print ("span:", span)
	
	spans_parsed.append ({
		"volume": span ["v"]
		
		"high": span ["h"],
		"low": span ["l"]
		
		"open": span ["o"],
		"close": span ["c"]
	})


#rich.print_json (data = spans)