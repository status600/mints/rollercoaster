
'''
	To see dependencies of dependencies,
	add to this roster.
'''
dependencies = [

]


import pkg_resources

def show_dependencies (module_name):
    distribution = pkg_resources.get_distribution (module_name)
    dependencies = distribution.requires ()

    print (f"Dependencies of {module_name}:")
    for dependency in dependencies:
        print (dependency)


	
show_dependencies ("tradingview-ta")